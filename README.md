# run-cpp-kate

Kate's External tool for compiling and executing cpp code in konsole

# Why this exist?
Default "Compile and Run Cpp" tool is unable to take input from users i.e `cin >> name`

# Installation
```sh
cd ~/.config/kate/externaltools
wget https://codeberg.org/Luciogi/run-cpp-kate/raw/branch/main/Compile_and_Run_in_Konsole.ini
```
(Restart kate if opened)

**Tip:** Add  in toolbar, (No keybaord shortcut available at the time)